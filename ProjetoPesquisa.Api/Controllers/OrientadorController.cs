﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjetoPesquisa.Application.Contracts.Aluno;
using ProjetoPesquisa.Application.Interfaces.Services;
using ProjetoPesquisa.Application.Services;

namespace ProjetoPesquisa.Api.Controllers
{
    /// <summary>
    /// Aluno Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrientadorController : BaseController
    {
        private readonly IAlunoService _alunoService;

        /// <summary>
        /// Construtor AlunoController
        /// </summary>
        /// <param name="alunoService"></param>
        public OrientadorController(IAlunoService alunoService)
        {
            _alunoService = alunoService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("Sobre")]
        public ContentResult About()
        {
            return Content("Faz toda a gestão do aluno no sistema.");
        }

        /// <summary>
        /// Lista Alunos
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<IActionResult> Get()
        {
            if (ModelState.IsValid)
            {
                return new OkObjectResult(new { Nome = "Danilo", Sobrenome = "Cáceres" });
            }
            return new OkObjectResult(new { Nome = "Danilo", Sobrenome = "Cáceres" });
        }

        /// <summary>
        /// Retorna Aluno
        /// </summary>
        /// <param name="ra"></param>
        /// <returns></returns>
        [HttpGet("{ra}")]
        public async Task<IActionResult> Get([FromRoute]string ra)
        {
            return await _alunoService.Autentica(new AlunoRequest { Ra = ra, Cpf = "12311" });
        }

        /// <summary>
        /// Insere Aluno
        /// </summary>
        /// <param name="aluno"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AlunoRequest aluno)
        {
            return Ok();
        }

        /// <summary>
        /// Autentica Aluno
        /// </summary>
        /// <param name="aluno"></param>
        /// <returns></returns>
        [HttpPost("Autentica")]
        public async Task<IActionResult> Autentica([FromBody] AlunoRequest aluno)
        {
            return Ok();
        }

        /// <summary>
        /// Altera Aluno
        /// </summary>
        /// <param name="id"></param>
        /// <param name="aluno"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody] AlunoRequest aluno)
        {

            if (ModelState.IsValid)
            {
                return new OkObjectResult(new { Nome = "Danilo", Sobrenome = "Cáceres" });
            }

            return Ok();
        }

        /// <summary>
        /// Remove Aluno
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok();
        }
    }
}
