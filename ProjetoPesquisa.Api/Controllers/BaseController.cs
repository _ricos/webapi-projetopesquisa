﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ProjetoPesquisa.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class BaseController : ControllerBase
    {
    }
}
