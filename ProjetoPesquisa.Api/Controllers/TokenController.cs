﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProjetoPesquisa.Application.Interfaces.Services;
using ProjetoPesquisa.Application.Settings;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ProjetoPesquisa.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly Jwt _jwtConfig;
        private readonly WebServices _webSvcConfig;
        private readonly IAlunoService _alunoService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="alunoService"></param>
        /// <param name="jwtConfig"></param>
        /// <param name="webSvcConfig"></param>
        public TokenController(IAlunoService alunoService,
            IOptions<Jwt> jwtConfig,
            IOptions<WebServices> webSvcConfig)
        {
            _alunoService = alunoService;
            _jwtConfig = jwtConfig.Value;
            _webSvcConfig = webSvcConfig.Value;
        }

        [AllowAnonymous]
        [HttpPost("Aluno")]
        public IActionResult AlunoToken([FromBody]LoginModel login)
        {
            var user = Authenticate(login);

            if (user != null)
            {
                var result = _alunoService.Autentica(new Application.Contracts.Aluno.AlunoRequest());
                var tokenString = BuildToken(user);
                return new OkObjectResult(new { token = tokenString });
            }

            return new UnauthorizedResult();
        }

        [AllowAnonymous]
        [HttpPost("Orientador")]
        public IActionResult Orientador([FromBody]LoginModel login)
        {
            var user = Authenticate(login);

            if (user != null)
            {
                var tokenString = BuildToken(user);
                return new OkObjectResult(new { token = tokenString });
            }

            return new UnauthorizedResult();
        }


        [AllowAnonymous]
        [HttpPost("Parecerista")]
        public IActionResult Parecerista([FromBody]LoginModel login)
        {
            var user = Authenticate(login);

            if (user != null)
            {
                var tokenString = BuildToken(user);
                return new OkObjectResult(new { token = tokenString });
            }

            return new UnauthorizedResult();
        }

        [AllowAnonymous]
        [HttpPost("PareceristaExterno")]
        public IActionResult PareceristaExterno([FromBody]LoginModel login)
        {
            var user = Authenticate(login);

            if (user != null)
            {
                var tokenString = BuildToken(user);
                return new OkObjectResult(new { token = tokenString });
            }

            return new UnauthorizedResult();
        }

        [AllowAnonymous]
        [HttpPost("SetorPesquisa")]
        public IActionResult SetorPesquisa([FromBody]LoginModel login)
        {
            var user = Authenticate(login);

            if (user != null)
            {
                var tokenString = BuildToken(user);
                return new OkObjectResult(new { token = tokenString });
            }

            return new UnauthorizedResult();
        }

        private string BuildToken(UserModel user)
        {
            var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Name),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim(JwtRegisteredClaimNames.Birthdate, user.Birthdate.ToString("yyyy-MM-dd")),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfig.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_jwtConfig.Issuer,
              _jwtConfig.Issuer,
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UserModel Authenticate(LoginModel login)
        {
            UserModel user = null;

            if (login.Username == "danilo" && login.Password == "senha")
            {
                user = new UserModel { Name = "Danilo Cáceres", Email = "danilo.caceres@domain.com" };
            }
            return user;
        }

        public class LoginModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        private class UserModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime Birthdate { get; set; }
        }
    }
}
