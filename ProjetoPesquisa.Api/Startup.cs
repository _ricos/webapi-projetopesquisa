﻿using Dapper.FluentMap;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProjetoPesquisa.Application.Contracts.Aluno;
using ProjetoPesquisa.Application.Interfaces.Repositories;
using ProjetoPesquisa.Application.Interfaces.Services;
using ProjetoPesquisa.Application.Repository;
using ProjetoPesquisa.Application.Repository.Contexts;
using ProjetoPesquisa.Application.Repository.Mappings;
using ProjetoPesquisa.Application.Services;
using ProjetoPesquisa.Application.Settings;
using ProjetoPesquisa.Application.Validators.Aluno;
using Swashbuckle.AspNetCore.Swagger;
using System.Data.SqlClient;
using System.Text;

namespace ProjetoPesquisa.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = "true",
                ValidAudience = "true",
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.cThIIoDvwdueQB468K5xDc5633seEFoqwxjF_xSJyQQ"))
            };
        });

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation();

            //// Swagger
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Unip Sistema de Pesquisa", Version = "v1" });
            });

            //// FluentMap
            InitializeMapping();

            //// ConnStrings
            services.Configure<ConnectionStrings>(Configuration.GetSection(nameof(ConnectionStrings)));


            //// Fluent Validation
            services.AddTransient<IValidator<AlunoRequest>, AlunoRequestValidator>();

            //// Services
            services.AddTransient<IAlunoService, AlunoService>();

            //// Repositories
            services.AddTransient<IAlunoRepository, AlunoRepository>();

            //// Context
            services.AddTransient(typeof(IcitecContext), x => new IcitecContext(new SqlConnection(x.GetService<IOptions<ConnectionStrings>>().Value.IcitecContext)));

            //// Settings Application
            services.Configure<Jwt>(Configuration.GetSection(nameof(Jwt)));
            services.Configure<WebServices>(Configuration.GetSection(nameof(WebServices)));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
                // specifying the Swagger JSON endpoint.
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Unip API");
                    c.RoutePrefix = string.Empty;
                });
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
        private void InitializeMapping()
        {
            FluentMapper.Initialize(config =>
            {
                config.AddMap(new AlunoDocenteMap());
            });
        }
    }
}
