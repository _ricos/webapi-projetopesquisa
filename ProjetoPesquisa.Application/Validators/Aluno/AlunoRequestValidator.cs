﻿using FluentValidation;
using ProjetoPesquisa.Application.Contracts.Aluno;

namespace ProjetoPesquisa.Application.Validators.Aluno
{
    public class AlunoRequestValidator : AbstractValidator<AlunoRequest>
    {
        public AlunoRequestValidator()
        {
            RuleFor(x => x.Ra).NotNull();
            RuleFor(x => x.Ra).MaximumLength(2).WithMessage(Resource.Campo_Obrigatorio);
            RuleFor(x => x.Ra).MaximumLength(2).WithMessage(Resource.Campo_Tamanho);
            RuleFor(x => x.Cpf).Length(0, 10);
        }
    }
}