﻿namespace ProjetoPesquisa.Application.Contracts.Aluno
{
    public class AlunoRequest
    {
        public string Ra { get; set; }
        public string Cpf { get; set; }  
    }
}
