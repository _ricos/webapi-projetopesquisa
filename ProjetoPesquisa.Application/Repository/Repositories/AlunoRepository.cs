﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjetoPesquisa.Application.Contracts.Aluno;
using ProjetoPesquisa.Application.Dto;
using ProjetoPesquisa.Application.Interfaces.Repositories;
using ProjetoPesquisa.Application.Repository.Contexts;
using ProjetoPesquisa.Application.Repository.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoPesquisa.Application.Repository
{
    public class AlunoRepository : BaseRepository<AlunoDocente>,IAlunoRepository
    {
        private readonly ILogger<AlunoRepository> _logger;
        public AlunoRepository(IcitecContext icitecContext, ILogger<AlunoRepository> logger) : base(icitecContext, logger)
        {
            _logger = logger;
        }

        public Task<int> ParaTestar(AlunoRequest aluno)
        {
            var res = ListaAsync(new AlunoDocente());
            return this.Quantidade("");
        }
    }
}
