﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjetoPesquisa.Application.Repository.Contexts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoPesquisa.Application.Repository.Repositories
{
    public abstract class BaseRepository<T> where T : class
    {
        protected readonly IcitecContext _context;
        protected readonly ILogger<BaseRepository<T>> _logger;
        protected BaseRepository (IcitecContext context, ILogger<BaseRepository<T>> logger)
        {
            _context = context;
            _logger = logger;
        }

        public virtual async Task<T> RetornaAsync(string value)
        {

            using (IDbConnection conn = _context.dbConn)
            {                
                try
                {
                    return await conn.QueryFirstOrDefaultAsync<T>(value);
                }
                catch (Exception ex)
                {
                    _logger.LogError("{class}| RetornaAsync --> Descrição: {erro}", "Class", ex.Message);
                    throw;
                }
            }
        }

        public virtual async Task<IEnumerable<T>> ListaAsync(T _obj)
        {
            using (IDbConnection conn = _context.dbConn)
                return await conn.GetListAsync<T>();
        }

        public virtual async Task<IEnumerable<T>> ListaPaginado(int numeroPg, int linhasPg, string condicao, string ordernar, object parametro)
        {
            condicao = "where Code like '%Invoice%'";
            ordernar = "Code desc";

            using (IDbConnection conn = _context.dbConn)
                return await conn.GetListPagedAsync<T>(numeroPg, linhasPg, condicao, ordernar, parametro);
        }

        public virtual async Task<int> Quantidade(string _condicao)
        {
            _condicao = "where ativo = 1";

            using (IDbConnection conn = _context.dbConn)
                return await conn.RecordCountAsync<T>(_condicao);
        }
    }
}
