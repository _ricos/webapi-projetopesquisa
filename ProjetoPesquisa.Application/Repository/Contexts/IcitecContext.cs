﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ProjetoPesquisa.Application.Repository.Contexts
{
    public class IcitecContext
    {
        public IDbConnection dbConn;
        public IcitecContext(IDbConnection dbConnection)
        {
            dbConn = dbConnection;
        }
    }
}
