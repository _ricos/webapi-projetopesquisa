﻿using Dapper.FluentMap.Mapping;
using ProjetoPesquisa.Application.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetoPesquisa.Application.Repository.Mappings
{
    public class AlunoDocenteMap : EntityMap<AlunoDocente>
    {
        public AlunoDocenteMap()
        {
            // Map property 'InvoiceID' to column 'Id'.
            Map(i => i.NumeroFuncionalDocente).ToColumn("Id");

            // Ignore the 'Detail' and 'Items' properties when mapping.
            Map(i => i.DataCriacao).Ignore();
        }
    }
}
