﻿using Microsoft.AspNetCore.Mvc;
using ProjetoPesquisa.Application.Contracts.Aluno;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoPesquisa.Application.Interfaces.Services
{
    public interface IAlunoService
    {
        Task<IActionResult> Autentica(AlunoRequest aluno);
    }
}
