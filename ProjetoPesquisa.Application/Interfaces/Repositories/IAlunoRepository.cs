﻿using Microsoft.AspNetCore.Mvc;
using ProjetoPesquisa.Application.Contracts.Aluno;
using ProjetoPesquisa.Application.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjetoPesquisa.Application.Interfaces.Repositories
{
    public interface IAlunoRepository : IBaseRepository<AlunoDocente>
    {
        Task<int> ParaTestar(AlunoRequest aluno);
    }
}
