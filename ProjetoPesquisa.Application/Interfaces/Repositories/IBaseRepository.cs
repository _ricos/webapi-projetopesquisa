﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoPesquisa.Application.Interfaces.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        Task<T> RetornaAsync(string value);

        Task<IEnumerable<T>> ListaAsync(T _obj);

        Task<IEnumerable<T>> ListaPaginado(int numeroPg, int linhasPg, string condicao, string ordernar, object parametro);

        Task<int> Quantidade(string _condicao);
    }
}
