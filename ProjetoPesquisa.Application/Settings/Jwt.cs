﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetoPesquisa.Application.Settings
{
    public class Jwt
    {
        public string Key { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
    }
}
