﻿namespace ProjetoPesquisa.Application.Settings
{
    public class WebServices
    {
        public CentralSistemaservico CentralSistemaservico { get; set; }
        public SetorPesquisa SetorPesquisa { get; set; }
    }

    public class CentralSistemaservico : WebServicesBase 
    {

    }

    public class SetorPesquisa : WebServicesBase
    {
       
    }
    public class WebServicesBase
    {
        public string Key { get; set; }
        public string Url { get; set; }
        public string SecurityMode { get; set; }
        public string CredentialType { get; set; }
    }
}
