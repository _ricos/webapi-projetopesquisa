﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetoPesquisa.Application.Dto
{
    public class AlunoDocente
    {
        public string NumeroFuncionalDocente { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Cpf { get; set; }
        public string Sexo { get; set; }
        public string InicioUnip { get; set; }//// TODO Verificar
        public DateTime DataCriacao { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public string CodStaProj { get; set; }//// TODO Verificar
        public string CodAvaliadorExterno { get; set; }//// TODO Verificar
    }
}
