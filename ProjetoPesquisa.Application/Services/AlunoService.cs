﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjetoPesquisa.Application.Contracts.Aluno;
using ProjetoPesquisa.Application.Interfaces.Repositories;
using ProjetoPesquisa.Application.Interfaces.Services;
using System.Threading.Tasks;

namespace ProjetoPesquisa.Application.Services
{
    public class AlunoService : IAlunoService
    {
        private readonly IAlunoRepository _alunoRepository;
        private readonly ILogger<IAlunoService> _logger;

        public AlunoService(IAlunoRepository alunoRepository, ILogger<IAlunoService> logger)
        {
            _alunoRepository = alunoRepository;
            _logger = logger;
        }

        public async Task<IActionResult> Autentica(AlunoRequest aluno)
        {
            _logger.LogWarning("Autentica aluno, ra:{Ra}", aluno.Ra);

            var _lst = await _alunoRepository.RetornaAsync("");
            var _aluno = await _alunoRepository.ParaTestar(aluno);

            if (string.IsNullOrEmpty(_lst.Cpf))
            {
                return new OkObjectResult(_aluno);
            }
            else
            {
                _logger.LogError("Não foi possível se autenticar no sistema, ra:{Ra}, nÃo localizado!", aluno.Ra);
                return new NotFoundObjectResult(_aluno);
            }
        }
    }
}
